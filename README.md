# **Network Scanner**

Easy to use network scanner in Python with flexible output, built-in scanning profiles, and IPS evasion features.

## **Download**

### Clone the repository

Clone the Git repository:

```
git clone https://gitlab.com/thart/networkscanner.git

cd networkscanner
```

### Install Python Prerequisites

```
sudo pip install requirements.txt
```

## **Usage**

Basic usage:

```
sudo python nsaas.py [options] target
```

If you need help or a list of available options:

```
sudo python nsaas.py --help
```

## **Examples**

Run a Ping sweep of the 192.168.88.0/24 network (both command options shown):

```
sudo python nsaas.py -i --target 192.168.88.0/24
sudo python nsaas.py --ping-sweep --target 192.168.88.0/24
```

Scanning for web servers inside 192.168.88.0/24 network:

```
sudo python nsaas.py --protocol tcp --port 80,443,8080 --target 192.168.88.0/24
```

Scanning for email servers inside 192.168.88.0/24 network:

```
sudo python nsaas.py --port email --target 192.168.88.0/24
```

## **Scan Types**

**-i, --ping-sweep**: Runs a simple ping sweep, does not scan ports

**TCP Scan**: [See the --protocols option](#scan-protocols)

**UDP Scan**: [See the --protocols option](#scan-protocols)

## **Scanning Options**

### Verbosity

**-v, --verbose**: Shows what the scanner is doing while it does it.

Verbose options: None

Example: --verbose

Default: Not verbose

### Output Formatting

**-o, --output**: Specify the output format. Default is JSON. 

Output options: CSV, JSON

Example: -o CSV

Default: JSON

### Probe Timeouts

**-t, --timeout**: How long to wait for a response from a port in seconds. 

Timeout options: Numeric value in seconds

Example: -t 2

Default: 1 second, then 3 seconds on reprobe.

### Scan Protocols

**-p, --protocol**: Which protocols to scan.

Protocol options: TCP, UDP

Example -p TCP,UDP

Default: TCP

### Ping First

**-f, --ping-first**: Ping target first and skip target if no reply, great time saver on large scans.

### Ports

**-l, --port**: Ports or pre-defined port list to scan. Default is "Top 20 Ports".

Port options: Single numeric port, comma-separated list (53,80,443), or pre-defined lists show below:

Pre-defined port lists:

* Web: 80, 8080, 443, 4443
* Email: 25, 2525, 465, 110, 995, 143, 993
* Mikrotik: 8728, 8729, 21, 22, 23, 8291, 80, 443, 53
* Admin: 22, 2222, 23, 2323, 3389, 3390, 5900, 5901, 5902
* Printing: 170, 515, 631, 9100
* Top_20: 20, 21, 22, 23, 25, 53, 69, 80, 110, 123, 137, 138, 139, 143, 161, 162, 389, 443, 636, 3389

Example: -l 53

Example: -l 53,80,443

Example: --port top_20

Default: top_20

## **Evasion Options**

### Port Randomization

**-q, --shuffle-ports**: Randomize ports while scanning.

Example --shuffle-ports

Default: None

### Target Randomization

**-r, --shuffle-targets**: Randomize targets while scanning.

Example --shuffle-targets

Default: None

### Sleepy Targets

**-s, --sleepy-targets**: Pause a random amount of time between min,max for each scan targets.

Example --sleepy-targets 1,4

Default: None

### Sleepy Ports

**-u, --sleepy-ports**: Pause a random amount of time between min,max for each port on a scan target.

Example --sleepy-ports 1,4

Default: None