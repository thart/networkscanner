# Copyright 2016, Manito Networks, LLC. All rights reserved
#/
# Last modified 8/21/2016

# Import what we need
import time, datetime, struct, sys, logging, logging.handlers, getopt, json, csv
from option_parser import * # Parse python input options, returns a {}
from scanner import ping_sweep,scanner # The scanners, returns a status {}
from ipaddress import * # pip install ipaddress
import logging_ops # Logging

# Set the logging level per https://docs.python.org/2/library/logging.html#levels
# Levels include DEBUG, INFO, WARNING, ERROR, CRITICAL (case matters)
logging.basicConfig(level=logging.INFO) # For logging to console in DEV
#logging.basicConfig(filename='/opt/manitonetworks/flow/ipfix.log',level=logging.WARNING) # For logging to a file in PROD
logger = logging.getLogger('Scanner')

### Get the input options, run the scanner, parse the output... ###

### Get the command line arguments ###
try:
    opts, args = getopt.getopt(sys.argv[1:],"ifqrhvo:a:p:l:t:s:u:",[
        "ping-sweep",
        "ping-first",
        "shuffle-ports",
        "shuffle-targets",
        "help",
        "verbose",
        "output=",
        "target=",
        "protocol=",
        "port=",
        "timeout=",
        "sleepy-targets=",
        "sleepy-ports="
        ])

except Exception,error_str:
    print error_str
    sys.exit("Unsupported or badly formed options, see --help for available arguments.") 

### Parse input options ###
try:
    input_options = option_parser(opts)
    print input_options

except Exception,options_str:
    print options_str
    sys.exit("Failed to parse input options, see --help for available arguments.")

### Done parsing command line arguments ###

### The scanners ###
try:
    if input_options["Ping Sweep"] == True: ### Check for Ping Sweep
        ping_sweep_result = ping_sweep(input_options["Target"],input_options["Verbose"])

    else: ### Run the full port scanner
        try:
            scan_result = scanner(
                input_options["Target"],
                input_options["Port List"],
                input_options["Protocol"],
                input_options["Timeout"],
                input_options["Verbose"],
                input_options) # Run the scanner

        except Exception,scanner_error:
            sys.exit("Failed to run the scan: " + str(scanner_error))

except KeyboardInterrupt:
    sys.exit("Scan interrupted by user, exiting.")

### End of the scanner ###

### Parse the output ###
try:
    if input_options["Output Format"].lower() == "json":
        print json.dumps(scan_result,sort_keys=True,indent=4)
    elif input_options["Output Format"].lower() == "csv":
        print "CSV Selected"
    else:
        pass
except Exception,output_error:
    print output_error
    sys.exit("Failed to parse output formatting, see --help for available --output arguments.")

### End of output parsing ###