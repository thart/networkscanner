import os,errno

# ICMP Type and Code Parser
def icmp_type_code(icmp_type,icmp_code):
    
    if icmp_type == 0:
        parsed_type = "Echo Reply"

    # ICMP Types 1,2 Unassigned    
    
    elif icmp_type == 3:      
        parsed_type = "Destination Unreachable"
        if icmp_code == 0:
            parsed_code = "Net Unreachable"
        elif icmp_code == 1: 
            parsed_code = "Host Unreachable"
        elif icmp_code == 2: 
            parsed_code = "Protocol Unreachable"
        elif icmp_code == 3:
            parsed_code = "Port Unreachable" 
        elif icmp_code == 4:
            parsed_code = "Fragmentation Needed and Don\'t Fragment was Set" 
        elif icmp_code == 5: 
            parsed_code = "Source Route Failed"
        elif icmp_code == 6: 
            parsed_code = "Destination Network Unknown"
        elif icmp_code == 7:
            parsed_code = "Destination Host Unknown" 
        elif icmp_code == 8: 
            parsed_code = "Source Host Isolated"
        elif icmp_code == 9: 
            parsed_code = "Communication with Destination Network is Administratively Prohibited"
        elif icmp_code == 10: 
            parsed_code = "Communication with Destination Host is Administratively Prohibited"
        elif icmp_code == 11: 
            parsed_code = "Destination Network Unreachable for Type of Service"
        elif icmp_code == 12:
            parsed_code = "Destination Host Unreachable for Type of Service"
        elif icmp_code == 13: 
            parsed_code = "Communication Administratively Prohibited"
        elif icmp_code == 14: 
            parsed_code = "Host Precedence Violation"
        elif icmp_code == 15: 
            parsed_code = "Precedence cutoff in effect"
        else:
            parsed_code = "None"
    
    elif icmp_type == 4:
        parsed_type = "Source Quench"
        if icmp_code == 0:
            parsed_code = "No Code"
    
    elif icmp_type == 5:
        parsed_type = "Redirect"
        if icmp_code == 0:
            parsed_code = "Redirect Datagram for the Network"
        if icmp_code == 1:
            parsed_code = "Redirect Datagram for the Host"
        if icmp_code == 2:
            parsed_code = "Redirect Datagram for the Type of Service and Network"
        if icmp_code == 3:
            parsed_code = "Redirect Datagram for the Type of Service and Host"    
    
    elif icmp_type == 6:
        parsed_type = "Alternate Host Address"
        if icmp_code == 0:
            parsed_code = "Alternate Address for Host"
    
    elif icmp_type == 8:
        parsed_type = "Echo"
        if icmp_code == 0:
            parsed_code = "No Code"
    
    elif icmp_type == 9:
        parsed_type = "Router Advertisement" 
        if icmp_code == 0:
            parsed_code = "Normal router advertisement"

    elif icmp_type == 10:
        parsed_type = "Router Selection"  
        if icmp_code == 0:
            parsed_code = "No Code"          
    
    elif icmp_type == 11:
        parsed_type = "Time Exceeded"
        if icmp_code == 0:
            parsed_code = "Time to Live exceeded in Transit"
        elif icmp_code == 1: 
            parsed_code = "Fragment Reassembly Time Exceeded"
        else:
            parsed_code = "None"

    elif icmp_type == 12:
        parsed_type = "Parameter Problem"

    elif icmp_type == 13:
        parsed_type = "Timestamp"
        if icmp_code == 0:
            parsed_code = "No Code"

    elif icmp_type == 14:
        parsed_type = "Timestamp Reply"
        if icmp_code == 0:
            parsed_code = "No Code"

    elif icmp_type == 15:
        parsed_type = "Information Request"
        if icmp_code == 0:
            parsed_code = "No Code"

    elif icmp_type == 16:
        parsed_type = "Information Reply"  
        if icmp_code == 0:
            parsed_code = "No Code"

    elif icmp_type == 17:
        parsed_type = "Address Mask Request"
        if icmp_code == 0:
            parsed_code = "No Code"

    elif icmp_type == 18:
        parsed_type = "Address Mask Reply" 
        if icmp_code == 0:
            parsed_code = "No Code"

    # ICMP Type 19-29 "reserved" for security and "robustness experiment"    

    elif icmp_type == 30:
        parsed_type = "Traceroute" 
        
    elif icmp_type == 31:
        parsed_type = "Datagram Conversion Error" 
        
    elif icmp_type == 32:
        parsed_type = "Mobile Host Redirect" 
        
    elif icmp_type == 33:
        parsed_type = "IPv6 Where-Are-You" 
        
    elif icmp_type == 34:
        parsed_type = "IPv6 I-Am-Here" 
        
    elif icmp_type == 35:
        parsed_type = "Mobile Registration Request" 
        
    elif icmp_type == 36:
        parsed_type = "Mobile Registration Reply" 
        
    elif icmp_type == 37:
        parsed_type = "Domain Name Request" 
        
    elif icmp_type == 38:
        parsed_type = "Domain Name Reply" 
        
    elif icmp_type == 39:
        parsed_type = "SKIP" 
        
    elif icmp_type == 40:
        parsed_type = "Photuris" 
        if icmp_code == 0:
            parsed_code = "Bad SPI"
        if icmp_code == 1:
            parsed_code = "Authentication Failed"
        if icmp_code == 2:
            parsed_code = "Decomposition Failed"
        if icmp_code == 3:
            parsed_code = "Decryption Failed"
        if icmp_code == 4:
            parsed_code = "Need Authentication"
        if icmp_code == 5:
            parsed_code = "Need Authorization"    

    # ICMP Types 41-255 reserved for future use    
        
    else: # Unknown ICMP Type
        parsed_type = "Unknown" 
        parsed_code = "None" 

    if parsed_code is None or icmp_code is None:
        parsed_code = "None"

    return {"ICMP Type":parsed_type,"ICMP Code":parsed_code,"Status":parsed_type}     

# Socket Error Parser
def socket_error(error_message):
    try:
        error_status = os.strerror(error_message.errno)
    except Exception,parse_error:
        print str(parse_error) + " - " + str(error_message)
        if "timed out" in error_message:
            error_status = "Timeout"
        elif "Connection refused" in error_message:
            error_status = "Filtered"
        elif "Network is unreachable" in error_message:
            error_status = "Unreachable"   
        else:
            error_status = "Unknown"
    
    return {"Error Status":error_status,"Error Message":str(error_message)}