import socket,random,os
from struct import *

def dns_probe(server,port,protocol,domain):
    
    ### Build the DNS query header ###
    query_string = {}
    
    query_id = os.urandom(2) # 2 Bytes

    query_string["Id"] = query_id
    
    # Query type
    # 0 = query
    # 1 = response
    query_qr = 0 # 1 bit
      
    # Authoritative Answer
    query_aa = 0 # 

    # Opcode
    # 0 = standard query
    # 1 = inverse query
    # 2 = server status request
    # 3-15 are reserved per RFC
    query_opcode = 0 # 4 bits
    
    # Truncation
    query_tc = 0 # 1 bit
    
    # Recursion desired
    query_rd = 1 # 1 bit

    # Z, reserved for future use
    query_z = 0

    # Non-authenticated data OK
    query_nonauth = 0
    
    #query_string["Flags"] = query_qr+query_opcode+query_tc+query_rd+query_z+query_nonauth
    query_string["Flags"] = pack('!bixxbxxxx',query_qr,query_opcode,query_rd)

    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    s.settimeout(2)

    try:
        s.connect((server,port))  
    except socket.error,socket_error:
        print "Unable to connect: " + str(socket_error)

    try:
        s.send(query_string["Id"] + query_string["Flags"])
        query["Status Response"] = s.recv(20)
    except socket.error,socket_error:
        print "Unable to receive: " + str(socket_error)

    return query_string

print dns_probe("192.168.88.1",53,"TCP","google.com")