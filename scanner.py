import socket, struct, errno, os, time, datetime, random, subprocess, sys

# Field types, ports, etc
from defined_ports import defined_ports,port_sets

# Logging time
from logging_ops import log_time

# Parsers for ICMP Types / Codes and socket error strings
from output_parsers import *

def verbose_print(message,verbose):
    if verbose is True:
        print(message)

def ping_target(target):
    try:
        # http://stackoverflow.com/questions/5596911/python-os-system-without-the-output
        with open(os.devnull, 'wb') as devnull:
            subprocess.check_call(['ping','-c','1',target], stdout=devnull, stderr=subprocess.STDOUT)
            return True
    except Exception,ping_error:
        return ping_error

def ping_sweep(target,verbose):

    status = {}
    
    target_count = len(target) # Total target count
    target_number = 0 # Current target number

    scan_begin_time = datetime.datetime.utcnow() # Scan start time
    status["Scan Begin"] = str(datetime.datetime.utcnow()) # Scan start time

    verbose_print("Scan started at " + str(status["Scan Begin"]),verbose)

    for current_ping_target in target:

        target_number += 1

        target_percentage = round(100*(float(target_number)/float(target_count)),2)
        verbose_print("Scanning target %s of %s - %s percent complete" % (target_number,target_count,target_percentage),verbose)

        status[str(current_ping_target)] = {}
        status[str(current_ping_target)]["Result"] = {}
        status[str(current_ping_target)]["Result"]["Started"] = str(datetime.datetime.utcnow()) + " UTC"

        ping_status = ping_target(str(current_ping_target)) # Run the ping

        if ping_status == True:
            verbose_print("Ping succeeded for " + str(current_ping_target),verbose)
            status[str(current_ping_target)]["Result"]["Status"] = "Online" # Target is online
            status[str(current_ping_target)]["Result"]["Ping"] = "Success" # Ping was successful

        else:
            verbose_print("Ping failed for " + str(current_ping_target),verbose)
            status[str(current_ping_target)]["Result"]["Status"] = "Offline" # Target is offline
            status[str(current_ping_target)]["Result"]["Ping"] = "Failed" # Ping was successful
            status[str(current_ping_target)]["Result"]["Finished"] = str(datetime.datetime.utcnow()) + " UTC"

    return status

def scanner(address,port,protocol,timeout,verbose,options):
    
    status = {}
    
    target_count = len(address) # Total target count
    target_number = 0 # Current target number
    
    scan_begin_time = datetime.datetime.utcnow() # Scan start time
    status["Scan Begin"] = str(datetime.datetime.utcnow()) # Scan start time
    
    verbose_print("Scan started at " + str(status["Scan Begin"]),verbose)

    if "Shuffle Targets" in options: # Shuffle targets
        random.shuffle(address)

    if "Shuffle Ports" in options: # Shuffle ports
        random.shuffle(port)

    for single_target in address: # Per target...
        target_number += 1

        if verbose is True:
            target_percentage = round(100*(float(target_number)/float(target_count)),2)
            verbose_print("Scanning target %s of %s - %s percent complete" % (target_number,target_count,target_percentage),verbose)
        
        status[str(single_target)] = {}
        status[str(single_target)]["Result"] = {}
        status[str(single_target)]["Result"]["Started"] = str(datetime.datetime.utcnow()) + " UTC"

        if "Sleepy Targets" in options: # Sleepy targets
            target_sleep_int = round(random.uniform(options["Sleepy Targets"]["Min"],options["Sleepy Targets"]["Max"]),2)
            verbose_print("Sleeping target for " + str(target_sleep_int),verbose)
            time.sleep(target_sleep_int) 
            status[str(single_target)]["Slept"] = target_sleep_int    

        if "Ping First" in options: # Ping first
            ping_status = ping_target(str(single_target))
            if ping_status == True:
                status[str(single_target)]["Result"]["Status"] = "Online" # Target is online
                status[str(single_target)]["Result"]["Ping"] = "Success" # Ping was successful
                pass
            else:
                verbose_print("Ping failed for " + str(single_target) + ", skipping...",verbose)
                status[str(single_target)]["Result"]["Outcome"] = "Skipped"
                status[str(single_target)]["Result"]["Scanned"] = False
                status[str(single_target)]["Result"]["Status"] = "Offline" # Target is offline
                status[str(single_target)]["Result"]["Ping"] = "Failed" # Ping was successful
                status[str(single_target)]["Result"]["Finished"] = str(datetime.datetime.utcnow()) + " UTC"
                continue

        for protocol_type in protocol: # Per protocol...
            status[str(single_target)][str(protocol_type)] = {}
            
            for single_port in port: # Per port...

                status[str(single_target)][str(protocol_type)][int(single_port)] = {}

                if "Sleepy Ports" in options: # Sleepy ports
                    port_sleep_int = round(random.uniform(options["Sleepy Ports"]["Min"],options["Sleepy Ports"]["Max"]),2)
                    verbose_print("Sleeping port for " + str(port_sleep_int),verbose)
                    time.sleep(port_sleep_int) 
                    status[str(single_target)][str(protocol_type)][int(single_port)]["Slept"] = port_sleep_int                       
                
                disposition = None # Reset final port status

                icmp = socket.getprotobyname('icmp') # Define ICMP protocol
                udp = socket.getprotobyname('udp') # Define UDP protocol
                
                # UDP
                if protocol_type.lower() == "udp":
                    icmp_recv_socket = socket.socket(socket.AF_INET, socket.SOCK_RAW, icmp) # ICMP reply listener
                    udp_send_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, udp) # UDP talker
                    icmp_recv_socket.bind(("", single_port)) # Listener for ICMP replies on the current port
                    icmp_recv_socket.settimeout(3) # Timeout for the ICMP listener
                     
                    try:
                        udp_send_socket.sendto("Hello\r\n", (single_target, single_port)) # Try waking up whatever service is on that port
                        rec_packet, curr_addr = icmp_recv_socket.recvfrom(1024) # Get something back
                        
                        if rec_packet:
                            icmp_header = rec_packet[20:28]
                            icmp_type, icmp_code, checksum, p_id, sequence = struct.unpack('bbHHh', icmp_header)
                            
                            icmp_info = icmp_type_code(icmp_type,icmp_code) # Parse ICMP Type and Code
                            status[str(single_target)][str(protocol_type)][int(single_port)]["ICMP Type"] = icmp_info["ICMP Type"]   
                            status[str(single_target)][str(protocol_type)][int(single_port)]["ICMP Code"] = icmp_info["ICMP Code"]
                            status[str(single_target)][str(protocol_type)][int(single_port)]["ICMP Responder"] = curr_addr[0] # ICMP Responder
                            disposition = icmp_info["Status"]
                            if curr_addr[0] == str(single_target):
                                status[str(single_target)]["Result"]["Status"] = "Online" # Target is online
                        
                        else:
                            disposition = "Unknown"
                    
                    except socket.timeout:
                        disposition = "Timeout"
                    
                    except socket.error,udp_socket_exception:
                        parsed_error = socket_error(udp_socket_exception)
                        disposition = parsed_error["Error Status"]
                   
                    finally: # Cleanup
                        udp_send_socket.close()
                        icmp_recv_socket.close()                    
                        status[str(single_target)][str(protocol_type)][int(single_port)]["Status"] = disposition    
                
                # TCP
                elif protocol_type.lower() == "tcp":
                                        
                    try: # Regular TCP SYN talker
                        tcp_send_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM) # TCP talker
                        tcp_send_socket.settimeout(timeout) # Set the timeout
                        tcp_send_socket.connect((single_target,single_port)) # Try to connect
                        tcp_send_socket.shutdown(1)
                        tcp_send_socket.close()
                        disposition = "Open"
                        status[str(single_target)]["Result"]["Status"] = "Online" # Target is online
                    
                    except: # Failed, so set up ICMP listener and TCP talker and try again
                        tcp_send_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM) # TCP talker
                        tcp_send_socket.settimeout(timeout) # Set the timeout
                        icmp_recv_socket = socket.socket(socket.AF_INET, socket.SOCK_RAW, icmp) # ICMP reply listener
                        icmp_recv_socket.bind(("", single_port)) # Listener for ICMP replies on the current port
                        icmp_recv_socket.settimeout(3) # Timeout for the ICMP listener
                        try:
                            tcp_send_socket.connect((single_target,single_port)) # Try to connect, will fail
                        except:
                            try:
                                rec_packet, curr_addr = icmp_recv_socket.recvfrom(1024) # ICMP reply
                                
                                if rec_packet:
                                    icmp_header = rec_packet[20:28]
                                    icmp_type, icmp_code, checksum, p_id, sequence = struct.unpack('bbHHh', icmp_header)
                                    icmp_info = icmp_type_code(icmp_type,icmp_code) # Parse ICMP Type and Code
                                    status[str(single_target)][str(protocol_type)][int(single_port)]["ICMP Type"] = icmp_info["ICMP Type"]   
                                    status[str(single_target)][str(protocol_type)][int(single_port)]["ICMP Code"] = icmp_info["ICMP Code"]
                                    status[str(single_target)][str(protocol_type)][int(single_port)]["ICMP Responder"] = curr_addr[0] # ICMP Responder
                                    disposition = icmp_info["Status"] 
                                    if curr_addr[0] == str(single_target):
                                        status[str(single_target)]["Result"]["Status"] = "Online" # Target is online
                            
                                else:
                                    disposition = "Unknown"
                            
                            except socket.timeout:
                                disposition = "Timeout"
                            
                            except socket.error,tcp_socket_exception:
                                parsed_error = socket_error(tcp_socket_exception)
                                status[str(single_target)][str(protocol_type)][int(single_port)]["Socket Error"] = str(tcp_socket_exception)
                                disposition = parsed_error["Error Status"]
                        finally:
                            icmp_recv_socket.close()
                    
                    finally: # Commit final port status
                        status[str(single_target)][str(protocol_type)][int(single_port)]["Status"] = disposition

                else:
                    sys.exit("Unsupported protocol, check --help for available protocols.") # Unknown protocol
                
                if int(single_port) in defined_ports:
                    try:
                        status[str(single_target)][str(protocol_type)][int(single_port)]["Name"] = defined_ports[int(single_port)]["Name"]
                    except:
                        pass
                    try:
                        status[str(single_target)][str(protocol_type)][int(single_port)]["Category"] = defined_ports[int(single_port)]["Category"]    
                    except:
                        pass
                else:
                    status[str(single_target)][str(protocol_type)][int(single_port)]["Category"] = "Other"

                if verbose is True:
                    print str(single_target).rjust(4),protocol_type.rjust(4),str(single_port).rjust(4),str(disposition).rjust(4)
        
        status[str(single_target)]["Result"]["Finished"] = str(datetime.datetime.utcnow()) + " UTC"
        status[str(single_target)]["Result"]["Scanned"] = True
        verbose_print("Finished target " + str(single_target),verbose)
    
    scan_end_time = datetime.datetime.utcnow()
    status["Scan End"] = str(scan_end_time)
    status["Time Elapsed"] = (scan_end_time - scan_begin_time).seconds
    
    if verbose is True:
        print "Scan finished at " + str(status["Scan End"])

    return status