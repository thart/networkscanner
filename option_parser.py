import sys
from defined_ports import defined_ports,port_sets
from ipaddress import *

def option_parser(options):
    option_output = {}
    
    # Supported protocols
    supported_protocols = ["TCP","UDP"]

    # For each option and argument
    for opt, arg in options:
        opt = opt.lower()
        arg = arg.lower()
        
        if opt in ('-a','--target'): # Targets
            try:
                target = []
                target_exploded = arg.split(",")
                for ipaddr in target_exploded: # Target option input exploded
                    
                    if "/" in ipaddr: # CIDR network
                        for host in ip_network(unicode(str(ipaddr),"utf-8")).hosts():
                            target.append(str(host))
                    
                    else: # Single host
                        target.append(unicode(str(ipaddr),"utf-8"))
                option_output["Target"] = target 
            
            except Exception,error_str:
                print error_str
                sys.exit("Please specify targets to scan e.g. 192.168.88.0/24 or 192.168.1.1,192.168.1.2")

        elif opt in ('-i','--ping-sweep'): # Ping sweep
            option_output["Ping Sweep"] = True
        
        elif opt in ('-f','--ping-first'): # Ping first
            option_output["Ping First"] = True

        elif opt in ('-p','--protocol'): # Protocol
            try:
                protocol = []
                option_output["Protocol"] = arg.split(",")
            except:
                sys.exit("Please specify supported protocols to scan e.g. TCP or TCP,UDP.")

        elif opt in ('-l','--port'): # Port list
            if arg.lower() in port_sets: # Pre-defined port lists
                port_list = port_sets[arg]
                option_output["Port List"] = port_list
            else:
                try:
                    port_list = []
                    port_exploded = arg.split(",")
                    for single_port in port_exploded:
                        port_list.append(int(single_port))
                    option_output["Port List"] = port_list
                except:
                    sys.exit("Bad port list; enter ports as comma-separated list e.g. 80,443,8080. See https://gitlab.com/thart/networkscanner#ports for a list of pre-defined port lists.")

        elif opt in ('-t','--timeout'): # Timeout (sec)
            option_output["Timeout"] = float(arg) 

        elif opt in ('-v','--verbose'): # Timeout (sec)
            option_output["Verbose"] = True  

        elif opt in ('-o','--output'): # Timeout (sec)
            if arg.lower() == "json":
                output_format = "json"  
            elif arg.lower() == "csv":
                output_format = "csv"  
            else:
                output_format = "json" # Default to JSON  
            
            option_output["Output Format"] = output_format
        
        elif opt in ('-q','--shuffle-ports'): # Evasion - Shuffle ports
            option_output["Shuffle Ports"] = True
        
        elif opt in ('-r','--shuffle-targets'): # Evasion - Shuffle targets
            option_output["Shuffle Targets"] = True 
        
        elif opt in ('-s','--sleepy-targets'): # Evasion - Random wait between targets
            try:
                arg_exploded = arg.split(",")
                option_output["Sleepy Targets"] = {}
                option_output["Sleepy Targets"]["Enabled"] = True
                option_output["Sleepy Targets"]["Min"] = float(arg_exploded[0])
                option_output["Sleepy Targets"]["Max"] = float(arg_exploded[1])
            except:
                sys.exit("Enter min and max for the sleepy targets pause in seconds, e.g. 1,4")

        elif opt in ('-u','--sleepy-ports'): # Evasion - Random wait between ports
            try:
                arg_exploded = arg.split(",")
                option_output["Sleepy Ports"] = {}
                option_output["Sleepy Ports"]["Enabled"] = True
                option_output["Sleepy Ports"]["Min"] = float(arg_exploded[0])
                option_output["Sleepy Ports"]["Max"] = float(arg_exploded[1])
            except:
                sys.exit("Enter min and max for the sleepy ports pause in seconds, e.g. 1,4")

        elif opt in ('-h','--help'):
            with open("./help.txt") as help_file:
                print help_file.read()
            sys.exit("See --help for arguments.")    

        else:
            sys.exit("Unknown argument, see --help for arguments.") 
    
    # Check if protocols are set, and if not use defaults
    try:
        option_output["Protocol"]
    except:
        option_output["Protocol"] = ["TCP"] # Defaults

    # Check if output format is set, and if not use JSON
    try:
        option_output["Output Format"]
    except:
        option_output["Output Format"] = "json" # Defaults    

    # Check if ports are set, and if not use defaults
    try:
        option_output["Port List"]
    except:
        option_output["Port List"] = port_sets["top_20"] # Defaults    

    # Check if timeout is set, and if not use default 1 sec.
    try:
        option_output["Timeout"]
    except:
        option_output["Timeout"] = 1.0 # Default

    # Check if Verbose is set, and if not use default
    try:
        option_output["Verbose"]
    except:
        option_output["Verbose"] = False # Default
    
    return option_output